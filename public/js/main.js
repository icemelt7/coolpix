$(".mobile-gallery").on("touchmove", HandleDrag);

var x_middle = $("body").width() / 2;
var y_middle = $("body").height() / 2;
var x_cur = 0;
var y_cur = 0;
var dx = 0;
var dy = 0;
var rotation_angle = 0;

function HandleDrag(e)
{
    var touch = e.originalEvent.touches[0];
    
    x_cur = touch.clientX;
    y_cur = touch.clientY;
    y_cur = (y_cur > y_middle) ? y_middle : y_cur;

    dx = x_cur - x_middle;
    dy = y_cur - y_middle;

    rotation_angle = Math.atan(dy / dx) * (180 / Math.PI);

    rotation_angle = (rotation_angle > 0 || (rotation_angle==0 && x_cur < x_middle)) ? rotation_angle -90 : rotation_angle + 90;
    
    rating = (rotation_angle > 0) ? rotation_angle + 90 : rotation_angle + 90;
    rating = Math.round(rating / 20) + 1;
    opacity = (rating<5)? rating * 0.2: (10 -rating) * 0.2;

    $('#rating').html(rating);
    $('.active').css({ transform: "rotateZ(" + rotation_angle + "deg)",
                    'opacity': opacity
                    });
    $('.second').css({ 'opacity': 1 - opacity + 0.35
                    });
}