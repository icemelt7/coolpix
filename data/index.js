var MongoClient = require('mongodb').MongoClient
, assert = require('assert');
var ObjectId = require('mongodb').ObjectId
// Connection URL
var url = 'mongodb://icemelt7:jojo3333@ds145183.mlab.com:45183/coolpix';

var dbToExport = null;
// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
    
    if (db){
        console.log("Connected successfully to server");
        dbToExport = db;
    }
});

var findUser = function(hexId, callback) {
    var collection = dbToExport.collection('users');
    console.log("Hitting db for user");
    collection.find({_id:ObjectId(hexId)}).toArray().then((docs) => {
        callback(docs[0]);
    })
}
var insertUser = function(user, callback) {
    // Get the documents collection
    var collection = dbToExport.collection('users');
    // Insert some documents
    console.log("Inserting user in db ");
    collection.findOneAndReplace({id:user.id}, user, {upsert: true}, function(err, result) {
        let id = result.value ? ObjectId(result.value._id).toHexString() : ObjectId(result.lastErrorObject.upserted).toHexString()
      callback(id);
    });
  }

var insertPicture = function(picture, user, cb) {
    var collection = dbToExport.collection('pictures');
    collection.insertOne({
        picture: picture,
        user_id: ObjectId(user._id)
    }).then((r) => {
        console.log(r)
        cb(r)
    }).catch((err) => { 
        console.log(err)
        cb(r)
    });
}

var getUserPictures = function(user, callback) {
    var collection = dbToExport.collection('pictures');
    collection.find({user_id: ObjectId(user._id)}).toArray().then(function(docs) {
        callback(docs);
    })
}
module.exports = { insertUser, findUser, insertPicture, getUserPictures }