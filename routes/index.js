const express = require('express');
const passport = require('passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn('/login_page');
const router = express.Router();
const insertPicture = require('../data/index').insertPicture;
const getUserPictures = require('../data/index').getUserPictures;
const env = {
  AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
  AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
  AUTH0_CALLBACK_URL:
    process.env.AUTH0_CALLBACK_URL || 'http://localhost:3000/callback'
};

/* GET home page. */
router.get('/', ensureLoggedIn, function(req, res, next) {
  console.log("loading index page");
  res.render('index', {
    user: req.user
  });
});
router.get('/login_page', function(req, res, next) {
  res.render('login_page');
});

router.get('/login', passport.authenticate('facebook', {scope: "public_profile"}));

router.post('/upload_picture', ensureLoggedIn, function(req, res, next) {
  insertPicture(req.body, req.user, () => {
    res.send({msg: "successful "})
  });
})
router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/callback',
passport.authenticate('facebook', { successRedirect: '/',
                                    failureRedirect: '/' }));

router.get('/failure', function(req, res) {
  var error = req.flash("error");
  var error_description = req.flash("error_description");
  req.logout();
  res.render('failure', {
    error: error[0],
    error_description: error_description[0],
  });
});

module.exports = router;
