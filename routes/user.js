const express = require('express');
const passport = require('passport');
const ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();
const router = express.Router();
const getUserPictures = require('../data/index').getUserPictures;

/* GET user profile. */
router.get('/', ensureLoggedIn, function(req, res, next) {


  var callBackFunction = function (pics) {
    pics = pics.map((pic) => {
      pic.picture.url = pic.picture.url.replace("/image/upload","/image/upload/w_269,h_289,c_fill");
      return pic;
    })
      res.render('profile', {
        user: req.user,
        special_class: 'profile',
        pics: pics
    })
  }

  
  getUserPictures(req.user, callBackFunction);
})

module.exports = router;
